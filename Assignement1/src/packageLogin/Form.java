package packageLogin;
import java.sql.*;  

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UnsafeServlet
 */
@WebServlet("/Login")
public class Form extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Form() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected  void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userID = request.getParameter("userID");
	    String password = request.getParameter("password");  
	    boolean subscribed=false;
	    String firstName="";
	    String passwordId="";
	    try {
	    	try {
			    	Class.forName("com.mysql.jdbc.Driver");  
			    	Connection con=DriverManager.getConnection(  
			    	"jdbc:mysql://localhost:3306/users","root","root");  
			    	Statement stmt=con.createStatement();  
			    	ResultSet rs=stmt.executeQuery("select * from user");
			    	System.out.print(rs.getStatement());
			    	while (rs.next())
			        {
			          int id = rs.getInt("iduser");
			          firstName = rs.getString("userName");
			          passwordId = rs.getString("password");
			          if(firstName.equals(userID)) {
			        	  subscribed=true;
			          }

			        }
			    	con.close(); 
	    	}catch(Exception e){ System.out.println(e);}  
	      response.setContentType("text/html");
	      PrintWriter writer = response.getWriter();
	     if(subscribed) {
	      writer.println("<html><body>");
	      writer.println("<p><u>Bentornato!</u> <br />"); 
	      writer.println("userID=" + firstName + "<br/>");
	      writer.println("</body></html>");
	     }
	     else {
	    	 writer.println("<html><body>");
		      writer.println("<p><u>Non sei iscritto!</u> <br />"); 
		      writer.println("</body></html>"); 
	     }
	     
	      writer.close();
	    
	     
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	}

}
